import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginGuard} from "./openaireLibrary/login/loginGuard.guard";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./harvester/harvester.module').then(m => m.HarvesterModule)
    // loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'harvest',
    redirectTo: '', pathMatch: 'full'
  },
  {
      path: 'reload',
      loadChildren: () => import('./reload/libReload.module').then(m => m.LibReloadModule),
      data: {hasSidebar: false, hasHeader: false}
  },
  // {   path: 'error',
  //     pathMatch: 'full',
  //     component: AdminErrorPageComponent,
  //     data: {hasSidebar: false}
  // },
  // {   path: '**',
  //     pathMatch: 'full',
  //     component: AdminErrorPageComponent
  // }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
