import {ChangeDetectorRef, Component, HostListener, OnInit} from '@angular/core';
import {MenuItem} from './openaireLibrary/sharedComponents/menu';
import {ActivatedRoute, Data, NavigationEnd, Params, Router} from '@angular/router';
import {EnvProperties} from './openaireLibrary/utils/properties/env-properties';
import {Header} from './openaireLibrary/sharedComponents/navigationBar.component';
import {LayoutService} from "./openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {properties} from "../environments/environment";
import {BehaviorSubject, Subscriber} from "rxjs";
import {SmoothScroll} from "./openaireLibrary/utils/smooth-scroll";
import {ConnectHelper} from './openaireLibrary/connect/connectHelper';
import {ConfigurationService} from './openaireLibrary/utils/configuration/configuration.service';
import {StringUtils} from "./openaireLibrary/utils/string-utils.class";
import {OpenaireEntities} from "./openaireLibrary/utils/properties/searchFields";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  title = 'Research Community Dashboard | Administrator';
  properties: EnvProperties = properties;
  params: BehaviorSubject<Params> = new BehaviorSubject<Params>(null);
  data: BehaviorSubject<Data> = new BehaviorSubject<Data>(null);
  hasSidebar: boolean = false;
  hasHeader: boolean = true;
  hasAdminMenu: boolean = false;
  isFrontPage: boolean = false;
  sideBarItems: MenuItem[] = [];
  backItem: MenuItem = null;
  menuItems: MenuItem[] = [];
  menuHeader: Header = {
    route: "/",
    url: null,
    title: "Default menu header",
    logoUrl: null,
    logoSmallUrl: null,
    position: 'center',
    badge: false
  };
  userMenuItems: MenuItem[] = [];
  loading: boolean = true;
  paramsResolved: boolean = false;
  innerWidth;
  private subscriptions: any[] = [];
  headerLogoUrl: string;
  headerUrl: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private smoothScroll: SmoothScroll,
              private layoutService: LayoutService,
              private configurationService: ConfigurationService) {
    this.subscriptions.push(this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let r = this.route;
        while (r.firstChild) {
          r = r.firstChild;
        }
        this.paramsResolved = true;
        this.params.next(r.snapshot.params);
        this.data.next(r.snapshot.data);
      }
    }));
  }
  
  ngOnInit() {
    // if (typeof document !== 'undefined' && window) {
    //   this.innerWidth = window.innerWidth;
    // }
    // this.subscriptions.push(this.layoutService.hasSidebar.subscribe(hasSidebar => {
    //   this.hasSidebar = hasSidebar;
    //   this.cdr.detectChanges();
    // }));
    // this.subscriptions.push(this.layoutService.hasHeader.subscribe(hasHeader => {
    //   this.hasHeader = hasHeader;
    //   this.cdr.detectChanges();
    // }));
    // this.subscriptions.push(this.layoutService.hasAdminMenu.subscribe(hasAdminMenu => {
    //   this.hasAdminMenu = hasAdminMenu;
    //   this.cdr.detectChanges();
    // }));
    // this.subscriptions.push(this.layoutService.isFrontPage.subscribe(isFrontPage => {
    //   this.isFrontPage = isFrontPage;
    //   this.cdr.detectChanges();
    // }));
    // this.layoutService.setSmallScreen((this.innerWidth && this.innerWidth <= 640));
    // this.layoutService.setOpen(!(this.innerWidth && this.innerWidth <= 640));
    // this.subscriptions.push(this.params.subscribe(params => {
    //   if (this.paramsResolved) {
    //     this.loading = true;
    //   }
    // }));
    // this.subscriptions.push(this.data.subscribe(data => {
    //   if(data && data.portal) {
    //     this.setProperties(data.portal);
    //     this.configurationService.initPortal(this.properties, this.properties.adminToolsCommunity);
    //   }
    // }));
  }
  
  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   if (this.layoutService.isSmallScreen && event.target.innerWidth > 1219) {
  //     this.layoutService.setSmallScreen(false);
  //   } else if (!this.layoutService.isSmallScreen && event.target.innerWidth < 1219) {
  //     this.layoutService.setSmallScreen(true);
  //     this.layoutService.setOpen(false);
  //   }
  // }
  
  public ngOnDestroy() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      }
    });
    this.layoutService.clearSubscriptions();
    this.smoothScroll.clearSubscriptions();
  }
  
  // setProperties(id, type = null){
  //   this.properties.adminToolsCommunity = id;
  //   if(type) {
  //     this.properties.adminToolsPortalType = type;
  //   } else {
  //     ConnectHelper.setPortalTypeFromPid(id);
  //   }
  //   this.configurationService.initPortal(this.properties, this.properties.adminToolsCommunity);
  // }
  //
  // public get open() {
  //   return this.layoutService.open;
  // }
  //
  // public get hover() {
  //   return this.layoutService.hover;
  // }
  //
  // private buildMenu() {
  //   this.userMenuItems = [];
  //   this.sideBarItems = [];
  //   this.headerLogoUrl = null;
  //   this.headerUrl = 'https://' + ((properties.environment !== 'production')?'beta.':'') + 'connect.openaire.eu';
  //   this.menuHeader = {
  //     route: null,
  //     url: this.headerUrl,
  //     title: "Monitor",
  //     logoUrl: 'assets/common-assets/logo-services/connect/small.svg',
  //     logoSmallUrl: "assets/common-assets/logo-services/connect/small.svg",
  //     position: 'left',
  //     badge: true,
  //     menuPosition: "center"
  //   };
  //   this.menuItems = [];
  //   this.menuItems.push(
  //     new MenuItem("about", "About", this.headerUrl + "/about/learn-how", "", false, [], [], {}, null, null, null, null, "_self",
  //       "internal", false,
  //       [
  //         new MenuItem("", "Learn the process", this.headerUrl + "/about/learn-how",  "", false, [], [], {}, null, null, null, null, "_self"),
  //       ]
  //     )
  //   );
  //   this.menuItems.push(
  //     new MenuItem("communities", "Communities", this.headerUrl + "/search/find/communities", "", false, [], [], {}, null, null, null, null, "_self")
  //   );
  //
  //   this.backItem = null;
  //   this.hasSidebar = this.hasSidebar && this.sideBarItems.length > 1;
  // }
}
