export class EOSCService {
  // id: string;
  name: string;
  description: string;
  webpage: string;

  // configuration template instance info
  baseurl: string;
  compatibilities: string[];
}