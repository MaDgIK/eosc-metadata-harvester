import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing';
import {ReactiveFormsModule} from '@angular/forms';
import {NavigationBarModule} from './openaireLibrary/sharedComponents/navigationBar.module';
import {BottomModule} from './openaireLibrary/sharedComponents/bottom.module';
import {ErrorModule} from './openaireLibrary/error/error.module';
import {TitleCasePipe} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoadingModule} from "./openaireLibrary/utils/loading/loading.module";
import {SideBarModule} from "./openaireLibrary/dashboard/sharedComponents/sidebar/sideBar.module";
import {SharedModule} from "./openaireLibrary/shared/shared.module";
import {ErrorInterceptorService} from "./openaireLibrary/error-interceptor.service";
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from "./openaireLibrary/timeout-interceptor.service";
import { HomeModule } from './home/home.module';
import {HarvesterModule} from "./harvester/harvester.module";

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    BottomModule, NavigationBarModule,
    ErrorModule,
    SharedModule,
    BrowserAnimationsModule, LoadingModule, SideBarModule,
    HomeModule, HarvesterModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    TitleCasePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true
    },
    [{provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true}],
    [{provide: DEFAULT_TIMEOUT, useValue: 30000}]
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
