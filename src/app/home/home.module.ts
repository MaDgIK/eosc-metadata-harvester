import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {HomeRoutingModule} from "./home-routing.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {OtherPortalsModule} from "../openaireLibrary/sharedComponents/other-portals/other-portals.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {NumbersModule} from "../openaireLibrary/sharedComponents/numbers/numbers.module";
import {
  AdvancedSearchInputModule
} from "../openaireLibrary/sharedComponents/advanced-search-input/advanced-search-input.module";
import {InputModule} from "../openaireLibrary/sharedComponents/input/input.module";
import {SearchInputModule} from "../openaireLibrary/sharedComponents/search-input/search-input.module";
import {HomeComponent} from "./home.component";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    HomeRoutingModule,
    // SEOServiceModule, OtherPortalsModule, IconsModule, NumbersModule,
    // AdvancedSearchInputModule, InputModule, SearchInputModule
  ],
  declarations: [
    HomeComponent
  ],
  providers:[
    PreviousRouteRecorder
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule {}
