import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {HarvesterRoutingModule} from "./harvester-routing.module";
import {HarvesterComponent} from "./harvester.component";
import {HarvesterService} from "./harvester.service";
import {InputModule} from "../openaireLibrary/sharedComponents/input/input.module";
import {PagingModule} from "../openaireLibrary/utils/paging.module";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {RecaptchaModule} from "ng-recaptcha";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule, RouterModule,
    HarvesterRoutingModule, InputModule, PagingModule, AlertModalModule, RecaptchaModule, LoadingModule, IconsModule
  ],
  declarations: [
    HarvesterComponent
  ],
  providers:[
    PreviousRouteRecorder, HarvesterService
  ],
  exports: [
    HarvesterComponent
  ]
})
export class HarvesterModule {}
