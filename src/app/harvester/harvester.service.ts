import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {map, switchMap} from "rxjs/operators";
import { Parser } from 'xml2js';

@Injectable({
  providedIn: 'root'
})
export class HarvesterService {

  constructor(private http: HttpClient) {
  }

  getAllTemplateInstances(): Observable<any> {
    let url = "https://beta.providers.eosc-portal.eu/api/configurationTemplateInstance/all?from=0&quantity=100";
    return this.http.get(url, {responseType: 'json'}).pipe(map(res => res));
  }

  // getAllSubprofiles(): Observable<any> {
  //   let url = "https://beta.providers.eosc-portal.eu/api/datasource/all?catalogue_id=all&suspended=false&quantity=100";
  //   // return this.http.get(url).pipe(map(res => res));
  //   return this.http.get(url, {responseType: 'json'}).pipe(map(res => res));
  // }

  getListOfServicesByIds(ids: string): Observable<any> {
    // let url = "https://beta.providers.eosc-portal.eu/api/service/byID/openaire.validator,eudat.b2find,lindatclariah-cz.lindatclariah-cz_repository";
    let url = "https://beta.providers.eosc-portal.eu/api/service/byID/"+ids;
    return this.http.get(url, {responseType: 'json'}).pipe(map(res => res));
    // return this.http.get(url, {responseType: 'text'}).pipe(map(res => res));
  }

  harvest(baseUrl: string, email: string = null, max: string = null, format: string = null): Observable<any> {
    let url: string = "https://ip-90-147-152-76.na2.garrservices.it/oai-collector-service/api/collect?oaiBaseUrl="+baseUrl
      +(max ? ("&max="+max) : "")
      +(format ? ("&oaiFormat="+format) : "")
      +(email ? ("&notificationEmail="+email) : "");
    console.log(url);
    return this.http.get(url, {responseType: 'json'}).pipe(map(res => res));
  }

  getFormats(baseUrl: string): Observable<any> {
    let url: string = baseUrl+"?verb=ListMetadataFormats";
    return this.http.get(url, {responseType: 'text'})
      .pipe(switchMap(xml => this.parseXML(xml)));
  }

  async parseXML(xmlData: string) {
    let response = null;
    const parser = new Parser();
    await parser.parseString(xmlData, (err, result) => {
      if (!err) {
        let formats = [];
        if(result.hasOwnProperty("OAI-PMH")) {
          if(result['OAI-PMH'].hasOwnProperty("ListMetadataFormats")) {
            formats = result['OAI-PMH']['ListMetadataFormats'];
          }
        }
        // return formats;
        response = formats;
      }
      return response;
    });
    return response;
  }

}