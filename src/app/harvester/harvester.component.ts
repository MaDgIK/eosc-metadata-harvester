import {ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {HarvesterService} from "./harvester.service";
import {EOSCService} from "../entities/eoscservice";
import {OpenaireEntities} from "../openaireLibrary/utils/properties/searchFields";
import {HelperFunctions} from "../openaireLibrary/utils/HelperFunctions.class";
import {properties} from "../../environments/environment";
import {FormBuilder, UntypedFormGroup, Validators} from "@angular/forms";
import {StringUtils} from "../openaireLibrary/utils/string-utils.class";
import {NotificationHandler} from "../openaireLibrary/utils/notification-handler";
import {Option} from "../openaireLibrary/sharedComponents/input/input.component";

@Component({
  selector: 'app-harvester',
  templateUrl: './harvester.component.html',
  styleUrls: ['./harvester.component.css']
})
export class HarvesterComponent {
  public allServices: EOSCService[] = [];
  public services: EOSCService[] = [];
  public _maxCharacters: number = 28;
  public compatibilityFilterValues: Array<{"compatibility": string, "selected": boolean}> = [];
  public selectedFilters: number = 0;

  public pageSize: number = 10;
  public page: number = 1;

  public recordsNum: number = 10;
  public formatsDisabled: boolean = true;
  public selectedFormats: Option[] = [];
  public selectedBaseUrl: string = "";
  public sending: boolean = false;
  public form: UntypedFormGroup;
  public modalClicked: boolean = false;
  public modalShowNext: boolean = true;
  @ViewChild('modal') modal;
  @ViewChild('customRecordsNum') customRecordsNum;

  constructor(private cdr: ChangeDetectorRef, private fb: FormBuilder, private harvesterService: HarvesterService) {}

  ngOnInit() {
    this.form = this.fb.group({
      email: this.fb.control('', [Validators.email, Validators.required]),
      // recaptcha: this.fb.control('', Validators.required),
      recordsNum: this.fb.control(1000, Validators.required),
      format: this.fb.control(null, Validators.required)
    });

    let servicesMap: Map<string, EOSCService> = new Map<string, EOSCService>();

    this.harvesterService.getAllTemplateInstances().subscribe(res => {
      // console.log(res);
      let instances = res['results'];

      let compatibilitiesSet: Set<string> = new Set<string>();
      let serviceIds: Set<string> = new Set<string>();

      let ids: string = "";
      instances.forEach(subProfile => {
        let serviceId = subProfile.resourceId;

        if(!serviceIds.has(serviceId)) {
          ids += ids.length > 0 ? ("," + serviceId) : serviceId;

          let service = new EOSCService();
          // service.id = serviceId;
          service.baseurl = subProfile.payload.baseURL;
          service.compatibilities = [];
          if(subProfile.payload.compatibility) {
            service.compatibilities.push(subProfile.payload.compatibility);
          }
          servicesMap.set(serviceId, service);
        } else if(subProfile.payload.compatibility) {
          servicesMap.get(serviceId).compatibilities.push(subProfile.payload.compatibility);
        }
        if (subProfile.payload.compatibility && !compatibilitiesSet.has(subProfile.payload.compatibility)) {
          compatibilitiesSet.add(subProfile.payload.compatibility);
          this.compatibilityFilterValues.push({"compatibility": subProfile.payload.compatibility, "selected": false});
        }

        serviceIds.add(serviceId);
      });

      compatibilitiesSet = null;
      // console.log("Subprofiles: ", this.subProfiles);

      this.harvesterService.getListOfServicesByIds(ids).subscribe(res => {
        // this.services = res;
        // console.log(this.services);

        for(let result of res) {
          let service = servicesMap.get(result.id);
          // console.log(service);
          service.name = result.name;
          service.description = result.description;
          service.webpage = result.webpage;
        }

        this.allServices = this.getValues(servicesMap);
        this.services = [...this.allServices];
      }, error => {});

    }, error => {});


    // this.harvesterService.getServiceById().subscribe(res => {
    //   // console.log("Get service by id: ", res);
    // }, error => {});
  }

  public initForm() {
    this.recordsNum = 1000;
    if(this.customRecordsNum) {
      this.customRecordsNum.nativeElement.checked = true;
    }
    this.selectedFormats = [];
    this.formatsDisabled = true;

    this.form.patchValue({'recordsNum': this.recordsNum, 'format': null});

    this.cdr.detectChanges();
  }

  public getValues(map): any[] {
    return Array.from(map.values());
  }

  public _formatTitle(title, length) {
    return (((title + " (" + length + ")").length > this._maxCharacters) ? (title.substring(0, (this._maxCharacters - (" (" + length + ")").length - ('...').length)) + "...") : title) + " (" + (length > 95 ? "100" : length) + ")";
  }

  public _formatName(value) {
    //let maxLineLength = 24;
    let maxLineLength = 35;

    //1 space after checkbox
    //3 space before number + parenthesis
    if (value.length + 1 > maxLineLength) {
      return value.substr(0, maxLineLength - 3 - 1) + '...';
    }

    return value;
  }

  public filterChange(selected: boolean) {
    selected ? this.selectedFilters++ : this.selectedFilters--;
    if(this.selectedFilters == 0 || this.selectedFilters == this.compatibilityFilterValues.length) {
      this.services = [...this.allServices];
    } else {
      this.services = this.allServices.filter(service => {
        for (let filter of this.compatibilityFilterValues) {
          if (filter.selected && service.compatibilities.includes(filter.compatibility)) {
            return true;
          }
        }
        return false;
      })
    }
  }

  public updatePage($event) {
    this.page = $event.value;
    HelperFunctions.scroll();
  }

  public openModal(name: string = "", baseUrl: string = "") {
    this.modalShowNext = true;
    this.modalClicked = true;

    this.selectedBaseUrl = baseUrl;
    this.sending = false;
    this.initForm();

    this.modal.previousButton = false;
    this.modal.cancelButton = false;
    this.modal.okButton = true;
    this.modal.okButtonText = "Next";
    this.modal.stayOpen = true;
    // this.modal.alertTitle = "Provide your email to get feedback";
    this.modal.alertTitle = name ? name : "Harvest";
    this.modal.open();

    let options: Option[] = [];
    this.harvesterService.getFormats(baseUrl).subscribe(res => {
      for(let response of res) {
        for(let format of response['metadataFormat']) {
          options.push({"label": format.metadataNamespace, "value": format.metadataPrefix});
        }
      }
      if(options.length > 0) {
        this.selectedFormats = options;
        this.form.patchValue({'format': null});
        this.formatsDisabled = false;
      } else {
        this.selectedFormats = [{label: 'Default (oai_dc)', value: 'oai_dc'}];
        this.form.patchValue({'format': "oai_dc"});
      }
      this.cdr.detectChanges();
    }, error => {
      this.selectedFormats = [{label: 'Default (oai_dc)', value: 'oai_dc'}];
      this.form.patchValue({'format':"oai_dc"});
      this.cdr.detectChanges();
    })
  }

  previousModalClicked(event) {
    // if event is true, it means it was just closed - if it is true, ok was clicked
    if(!event) {
      this.modalShowNext = true;
      this.modal.previousButton = false;
      this.modal.stayOpen = true;
      this.modal.okButtonText = "Next";
    }
  }

  updateRecordsNum(increase: boolean = true) {
    this.recordsNum = this.recordsNum + (increase ? 1000 : -1000);
    this.form.get('recordsNum').setValue(this.recordsNum);
    this.customRecordsNum.nativeElement.checked = true;
    this.cdr.detectChanges();
  }

  public harvest() {
    if(this.modalShowNext) {
      this.modalShowNext = false;
      this.modal.previousButton = true;
      this.modal.stayOpen = false;
      this.modal.okButtonText = "Harvest";
      return;
    }

    this.sending = true;
    let email: string = this.form.get("email").value;
    if(!StringUtils.validateEmails(email)) {
      email = null;
    }
    let max: string = this.form.get("recordsNum").value;
    let oaiFormat: string = this.form.get("format").value;

    this.harvesterService.harvest(this.selectedBaseUrl, email, max, oaiFormat).subscribe(res => {
      console.log("Harvest: ", res);
      this.sending = false;
      NotificationHandler.rise('Harvest has begun');
    }, error => {
      this.sending = false;
      NotificationHandler.rise('An error occured', "danger");
    });
  }

  public handleRecaptcha(captchaResponse: string) {
    this.form.get('recaptcha').setValue(captchaResponse);
  }

  protected readonly properties = properties;
}
