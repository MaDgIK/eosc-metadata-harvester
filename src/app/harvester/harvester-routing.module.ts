import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{HarvesterComponent} from './harvester.component';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: HarvesterComponent, canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class HarvesterRoutingModule { }
