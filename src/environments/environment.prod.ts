import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';

export let properties: EnvProperties = {
  environment:"production"
};
